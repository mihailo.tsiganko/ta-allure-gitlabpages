package example.common.config;

import org.aeonbits.owner.Config;


@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "file:${env.config.path}",
        "file:${common.config.path}",
        "system:properties"
})
public interface ConfigProps extends Config {
    @Key("web.url")
    String webUrl();

    @Key("api.url")
    String apiUrl();

    @Key("driver.timeout")
    long driverTimeout();

    @Key("driver.type")
    String driverType();

    @Key("browser.headless")
    boolean browserHeadless();

    @Key("browser.width")
    int browserWidth();

    @Key("browser.height")
    int browserHeight();

    @Key("browser.downloads.collection")
    String browserDownloadCollection();

    @Key("info.url")
    String infoUrl();

    @Key("remote.driver")
    boolean remoteDriver();

    @Key("hub.url")
    String hubUrl();

}