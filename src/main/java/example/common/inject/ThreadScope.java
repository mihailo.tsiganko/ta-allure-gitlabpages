package example.common.inject;

import com.google.inject.Key;

import java.util.HashMap;
import java.util.Map;

public class ThreadScope {
    private ThreadLocal<Map<Key<?>,Object>> store = new ThreadLocal<>();

    @SuppressWarnings("unchecked")
    public  Object get(Key key) {
        if (store.get()==null){
            store.set(new HashMap<>());
        }
        return store.get().get(key);
    }

    public  void set(Key key, Object instance) {
        if(store.get() == null){
            store.set(new HashMap<>());
        }
        store.get().put(key,instance);
    }

    public void leave(){
        store.set(null);
    }

}
