package example.common.inject;


import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GuiceTestScope implements Scope {
    private GuiceTestScope(){

    };
    public static GuiceTestScope INSTANCE = new GuiceTestScope();

    private ThreadScope threadScope = new ThreadScope();
    private Map<Key<?>,Object> proxyStore = new HashMap<>();


    @Override
    public <T> Provider<T> scope(Key<T> key, Provider<T> unscoped) {
        return () -> {

            // Lookup instance
            var proxyInstance = proxyStore.get(key);
            var objectKey = (Key) key;
            var unscopedProvider = (Provider) unscoped;

            if (proxyInstance == null) {

                // Create instance
                proxyInstance = Enhancer.create((Class) objectKey.getTypeLiteral().getType(),
                        new InvocationHandler() {
                    @Override
                    public Object invoke(Object o, Method method, Object[] objects) throws InvocationTargetException, IllegalAccessException {
                        if(Objects.isNull(threadScope.get(objectKey))){
                            threadScope.set(objectKey,unscopedProvider.get());
                        }
                        var object = threadScope.get(objectKey);
                        return method.invoke(object, objects);
                    }
                }
                );
                proxyStore.put(key,proxyInstance);

            }
            return (T)proxyInstance;

        };
    }


    public void leave() {
        threadScope.leave();
    }


}
