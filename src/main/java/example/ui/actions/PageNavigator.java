package example.ui.actions;


import com.codeborne.selenide.Selenide;
import example.common.config.ConfigProvider;
import io.qameta.allure.Step;

public class PageNavigator {
    public static PageNavigator INSTANCE = new PageNavigator();

    @Step("User Opens search page")
    public void openSearchPage(){
        Selenide.open(ConfigProvider.CONFIG_PROPS.webUrl() + "search");
    }

    @Step("User Opens Home page")
    public void openHomePage(){
        Selenide.open(ConfigProvider.CONFIG_PROPS.webUrl() );
    }
}
