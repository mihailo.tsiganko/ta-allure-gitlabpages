package example.ui.verifications;

import com.codeborne.selenide.Condition;
import com.google.inject.Inject;
import example.ui.pages.WikipediaMainPage;
import io.qameta.allure.Step;

public class WikipediaVerifications {
    @Inject
    private WikipediaMainPage wikipediaMainPage;

    @Step("Verify main page tab active")
    public void verifyMainPageTabActive(){
        wikipediaMainPage.getMainPageTab().shouldHave(Condition.cssClass("selected"));
    }
}
