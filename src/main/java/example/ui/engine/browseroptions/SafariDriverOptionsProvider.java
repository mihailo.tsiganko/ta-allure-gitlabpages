package example.ui.engine.browseroptions;

import org.openqa.selenium.safari.SafariOptions;

public class SafariDriverOptionsProvider implements DriverOptionsProvider {

    @Override
    public SafariOptions getOptions() {
        return new SafariOptions();
    }
}
