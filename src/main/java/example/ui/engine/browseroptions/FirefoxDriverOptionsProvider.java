package example.ui.engine.browseroptions;

import org.openqa.selenium.firefox.FirefoxOptions;

import static example.common.config.ConfigProvider.CONFIG_PROPS;
import static java.lang.String.format;


public class FirefoxDriverOptionsProvider implements DriverOptionsProvider {

    @Override
    public FirefoxOptions getOptions() {
        var firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(CONFIG_PROPS.browserHeadless());
        firefoxOptions.addArguments(format("--width=%d", CONFIG_PROPS.browserWidth()));
        firefoxOptions.addArguments(format("--height=%d", CONFIG_PROPS.browserHeight()));
        return firefoxOptions;
    }
}
