package example.ui.engine.browseroptions;

import example.common.config.ConfigProvider;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeDriverOptionsProvider implements DriverOptionsProvider {

    @Override
    public EdgeOptions getOptions() {

        EdgeOptions options = new EdgeOptions();

        options.setPageLoadStrategy(PageLoadStrategy.NONE.toString());
        options.setCapability("headless", String.valueOf(ConfigProvider.CONFIG_PROPS.browserHeadless()));
        options.setCapability("userChromium", "true");
        return options;
    }
}
