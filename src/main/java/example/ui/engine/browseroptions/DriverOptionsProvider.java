package example.ui.engine.browseroptions;

import org.openqa.selenium.MutableCapabilities;

public interface DriverOptionsProvider {
    MutableCapabilities getOptions();
}
