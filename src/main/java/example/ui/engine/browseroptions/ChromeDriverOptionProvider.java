package example.ui.engine.browseroptions;

import example.ui.engine.DriverDownloadDirManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;

import static example.common.config.ConfigProvider.CONFIG_PROPS;
import static java.lang.String.format;

public class ChromeDriverOptionProvider implements DriverOptionsProvider {

    public ChromeOptions getOptions() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("download.default_directory", DriverDownloadDirManager.clearSetGet().toAbsolutePath().toString());
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.NONE);
        options.setHeadless(CONFIG_PROPS.browserHeadless());
        options.addArguments("disable-infobars");
        options.addArguments("--disable-notifications");
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
//        options.addArguments("--remote-debugging-port=9222"); //Options to debug headless chrome
        options.addArguments(format("--window-size=%d,%d", CONFIG_PROPS.browserWidth(), CONFIG_PROPS.browserHeight()));
        options.setExperimentalOption("prefs", chromePrefs);
        return options;
    }
}
