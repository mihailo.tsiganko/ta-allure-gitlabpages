package example.ui.engine;

import com.codeborne.selenide.Configuration;
import example.ui.engine.browseroptions.ChromeDriverOptionProvider;
import example.ui.engine.browseroptions.EdgeDriverOptionsProvider;
import example.ui.engine.browseroptions.FirefoxDriverOptionsProvider;
import example.ui.engine.browseroptions.SafariDriverOptionsProvider;
import org.openqa.selenium.MutableCapabilities;

import java.time.Duration;

import static example.common.config.ConfigProvider.CONFIG_PROPS;
import static java.lang.String.format;

public class DriverConfigurator {
    public static void configure() {
        Configuration.timeout = Duration.ofSeconds(CONFIG_PROPS.driverTimeout()).toMillis();
        Configuration.reopenBrowserOnFail = true;
        Configuration.savePageSource = false;
        Configuration.screenshots = false;
        Configuration.browserSize = format("%dx%d", CONFIG_PROPS.browserWidth(), CONFIG_PROPS.browserHeight());
//        Configuration.browserCapabilities = getDriverOptions();
        Configuration.browser = DriverType.getFromConfig().name().toLowerCase();
        Configuration.headless = CONFIG_PROPS.browserHeadless();
        if(CONFIG_PROPS.remoteDriver()){
            Configuration.remote = CONFIG_PROPS.hubUrl();
        }
    }

    private static MutableCapabilities getDriverOptions() {
        DriverType driverType = DriverType.getFromConfig();
        switch (driverType) {
            case CHROME:
                return new ChromeDriverOptionProvider().getOptions();
            case FIREFOX:
                return new FirefoxDriverOptionsProvider().getOptions();
            case EDGE:
                return new EdgeDriverOptionsProvider().getOptions();
            case SAFARI:
                return new SafariDriverOptionsProvider().getOptions();
            default:
                throw new IllegalArgumentException(format("No implementation for provided driver type: " +
                        "Driver Type[%s]", driverType));
        }
    }
}
