package example.ui.engine;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverBinaryManager {

    public static void setupWebDriverBinary() {
        DriverType driverType = DriverType.getFromConfig();
        switch (driverType) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                break;
            case EDGE:
                WebDriverManager.edgedriver().setup();
                break;
            case SAFARI:
                //safari driver is shipped along with safari browser
                break;
            default:
                throw new IllegalArgumentException(String.format("No implementation for provided driver type: " +
                        "Driver Type[%s]", driverType));
        }
    }
}
