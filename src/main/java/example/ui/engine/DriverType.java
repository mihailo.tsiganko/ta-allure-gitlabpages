package example.ui.engine;

import example.common.config.ConfigProvider;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum DriverType {
    CHROME, FIREFOX, EDGE, SAFARI;

    public static DriverType getFromConfig() {
        String driverType = ConfigProvider.CONFIG_PROPS.driverType();
        return Arrays.stream(DriverType.values())
                .filter(it -> it.name().equalsIgnoreCase(driverType))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Provided driver type is not supported\n" +
                        "Driver Type: " + driverType));
    }
}
