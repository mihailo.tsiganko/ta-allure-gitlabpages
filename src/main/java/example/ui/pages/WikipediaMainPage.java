package example.ui.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class WikipediaMainPage {
    private SelenideElement mainPageTab = $("#ca-nstab-main");
}
