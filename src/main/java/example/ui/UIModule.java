package example.ui;

import com.google.inject.AbstractModule;
import example.common.inject.GuiceTestScope;
import example.common.inject.TestScoped;

public class UIModule extends AbstractModule {
    @Override
    public void configure(){
        bindScope(TestScoped.class,
                GuiceTestScope.INSTANCE);
    }

}
