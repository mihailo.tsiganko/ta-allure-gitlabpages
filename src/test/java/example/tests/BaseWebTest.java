package example.tests;

import com.codeborne.selenide.Selenide;
import example.common.config.ConfigProvider;
import example.common.inject.GuiceTestScope;
import example.ui.UIModule;
import example.ui.engine.DriverBinaryManager;
import example.ui.engine.DriverConfigurator;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

@Guice(modules = {UIModule.class})
public class BaseWebTest {

    @BeforeSuite(alwaysRun = true, description = "Download web driver binaries")
    protected void beforeSuite(ITestContext context) {
        DriverBinaryManager.setupWebDriverBinary();//download web driver binary
    }

    @BeforeMethod(alwaysRun = true, description = "Initialize web driver configuration")
    protected void beforeMethod() {
        DriverConfigurator.configure();//this method doesn't open the browser yet
    }

    @AfterMethod(alwaysRun = true, description = "Close driver")
    protected void tearDown() {
        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();
        GuiceTestScope.INSTANCE.leave();
    }

    @AfterSuite(alwaysRun = true)
    public void setAllureEnvProperties() throws IOException {
        addEnvDetails();
    }

    private void addEnvDetails() throws IOException {
        var properties = new Properties();
        properties.put("BASE_URL", ConfigProvider.CONFIG_PROPS.webUrl());
        properties.put("BROWSER", ConfigProvider.CONFIG_PROPS.driverType().toUpperCase());
        properties.put("OS", System.getProperty("os.name"));
        var file = new File("target/allure-results/environment.properties");
        properties.store(new FileOutputStream(file), null);

    }
}
