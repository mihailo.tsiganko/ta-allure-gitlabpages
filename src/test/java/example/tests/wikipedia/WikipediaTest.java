package example.tests.wikipedia;

import com.google.inject.Inject;
import example.tests.BaseWebTest;
import example.ui.actions.PageNavigator;
import example.ui.verifications.WikipediaVerifications;
import io.qameta.allure.Step;
import org.testng.annotations.Test;

public class WikipediaTest extends BaseWebTest {
    @Inject
    private WikipediaVerifications wikipediaVerifications;

    @Test
    public void test(){
        PageNavigator.INSTANCE.openHomePage();
        wikipediaVerifications.verifyMainPageTabActive();
    }


    @Step
    public void doSomeStaff(){

    }

}
